// const helmet = require('helmet'); //security
// app.use(helmet());

const express = require('express');
const bodyParser = require('body-parser');
const { mongoose } = require('./server/mongoosedb');


const app = express();
const path = require("path");
app.use(bodyParser.json());

app.use(function(req, res, next) {
  res.header("Access-Control-Allow-Origin" , "*");
  res.header("Access-Control-Allow-Headers" , "Origin, X-Requested-With, Content-Type, Accept");
  next();
});

// article controller
require('./app/articles/articles.controller.js')(app);
// users controller
require('./app/users/users.controller.js')(app);


app.listen(3000, () => {
  console.log('started at 3000');
});

// const passport = require('passport');
// const LocalStrategy = require('passport-local');
//
// app.use(passport.initialize());
// app.use(passport.session());

// const {Users} = require('./app/users/users.model.js');

// passport.use(new LocalStrategy({
//   usernameField: 'user[email]',
//   passwordField: 'user[password]',
// }, (email, password, done) => {
//   User.findOne({ email })
//   .then((user) => {
//     if(!user || !user.validatePassword(password)) {
//       return done(null, false, { errors: { 'email or password': 'is invalid' } });
//     }
//
//     return done(null, user);
//   }).catch(done);
// }));


// app.post('/login' , passport.authenticate('local', { failureRedirect: '/login' }),  (req , res) => {
//
//     // res.redirect('/success?username='+req.user.email);
//     // res.redirect('/loged');
//     res.send('xxx');
//
// });

// app.post('/add-user', (req , res) => {
//   const newUser = new User({
//     email: req.body.email,
//     password: req.body.password,
//   })
//   newUser.save().then( addedUser => {
//     res.send(addedUser);
//   }, err => { res.send(err); });
// });
//
//
// app.get('/get-users', (req , res) => {
//   User.find().then((user)=> {
//     res.send(user);
//     console.log('user -> ', user);
//   }, (err) => {
//     console.log('error -> ', err);
//     res.send(err);
//   })
// });
