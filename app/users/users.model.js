
const config = require('./config.json');
const jwt = require('jsonwebtoken');
const Joi = require('joi');
const mongoose = require('mongoose');

//simple schema
const UserSchema = new mongoose.Schema({
  name: {
    type: String,
    required: true,
    minlength: 3,
    maxlength: 50
  },
  email: {
    type: String,
    required: true,
    minlength: 5,
    maxlength: 255,
    unique: true
  },
  password: {
    type: String,
    required: true,
    minlength: 3,
    maxlength: 255
  },
  isAdmin: {
    type: Boolean,
  }
  //give different access rights if admin or not
});


//custom method to generate authToken
UserSchema.methods.generateAuthToken = function() {
  const token = jwt.sign({ _id: this._id, isAdmin: this.isAdmin }, config.myprivatekey ); //get the private key from the config file -> environment variable
  return token;
}

const User = mongoose.model('User', UserSchema);

//function to validate user
function validateUser(user) {
  const schema = {
    name: Joi.string().min(3).max(50).required(),
    email: Joi.string().min(5).max(255).required().email(),
    password: Joi.string().min(3).max(255).required(),
  };

  return Joi.validate(user, schema);
}

exports.User = User;
exports.validate = validateUser;



// var mongoose = require('mongoose');
// const crypto = require('crypto');
// const jwt = require('jsonwebtoken');
//
// const { Schema } = mongoose;
//
// // const UsersModel = new Schema({
// //   email: {
// //     type: String,
// //     required: true,
// //     unique: true
// //   },
// //   password: {
// //     type: String
// //   },
// //   hash: String,
// //   salt: String
// // });
//
// var UsersModel = {
//   email: {
//     type: String,
//     required: true,
//     unique: true
//   },
//   password: {
//     type: String,
//     required: true,
//   },
//   // hash: String,
//   // salt: String
// };
//
// // UsersModel.methods.setPassword = function(password) {
// //   this.salt = crypto.randomBytes(16).toString('hex');
// //   this.hash = crypto.pbkdf2Sync(password, this.salt, 10000, 512, 'sha512').toString('hex');
// // };
// //
// // UsersModel.methods.validatePassword = function(password) {
// //   const hash = crypto.pbkdf2Sync(password, this.salt, 10000, 512, 'sha512').toString('hex');
// //   return this.hash === hash;
// // };
// //
// // UsersModel.methods.generateJWT = function() {
// //   const today = new Date();
// //   const expirationDate = new Date(today);
// //   expirationDate.setDate(today.getDate() + 60);
// //
// //   return jwt.sign({
// //     email: this.email,
// //     id: this._id,
// //     exp: parseInt(expirationDate.getTime() / 1000, 10),
// //   }, 'secret');
// // }
// //
// // UsersModel.methods.toAuthJSON = function() {
// //   return {
// //     _id: this._id,
// //     email: this.email,
// //     token: this.generateJWT(),
// //   };
// // };
//
// // const Users = mongoose.model('Users');
// var User = mongoose.model('Users', UsersModel );
//
//
// module.exports = { User };
