
const config = require('./config.json');
const auth = require("./users.auth.js");
const { User, validate } = require("./users.model.js");
const bcrypt = require("bcrypt");

module.exports = function(app) {

  // user register
  // register - just add user
  // authenticate
  // active (current)

  app.get("/user/current", auth, async (req, res) => {
    const user = await User.findById(req.user._id).select("-password");
    res.send(user);
  });

  app.get("/users/all", function(req, res) {
    User.find().then((_users)=>{
       console.log('all users -> ', _users);
       res.send(_users);
     });
  });

  // app.get("/login", function(req, res) {
  //   // User.find().then((_users)=>{
  //   //    console.log('all users -> ', _users);
  //   //    res.send(_users);
  //   //  });
  // });

  app.post("/user/add", async (req, res) => {
    // validate the request body first
    const { error } = validate(req.body);
    if (error) return res.status(400).send(error.details[0].message);

    //find an existing user
    let user = await User.findOne({ email: req.body.email });
    if (user) return res.status(400).send("User already registered.");

    user = new User({
      name: req.body.name,
      password: req.body.password,
      email: req.body.email,
      isAdmin: config.admin.includes(req.body.email) ? true : undefined,
    });
    // user.password = await bcrypt.hash(user.password, 10);
    await user.save();

    const token = user.generateAuthToken();
    res.header("x-auth-token", token).send({
      _id: user._id,
      name: user.name,
      email: user.email,
      isAdmin: user.isAdmin
    });
  });

}
