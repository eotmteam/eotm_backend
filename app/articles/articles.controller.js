const { Articles} = require('./articles.model.js');
// const dataCollections =  { live: ArticleLive,  ready: ArticleReady, all: ArticleAll };

const { ArticleTags } = require('./tags/tags.model.js')
const { ArticleTagsService } = require('./tags/tags.service.js')

module.exports = function(app) {

  require('./tags/tags.controller.js')(app, ArticleTagsService);

  app.post('/article/delete', (req, res) => {
    let aHref = req.body.href;
    Articles.deleteOne( { href: aHref }).then(
      suc => {
        res.send(suc);
      },
      err => {
        res.send(err);
      }
    );
  });

  const newArticleCreate = (_data) => {
    return new Articles({
      href: _data.href,
      category: _data.category,
      subcategory: _data.subcategory,
      headline: _data.headline,
      tags: _data.tags
    });
  }

  app.post('/articles/add',(req, res) => {
    var newArticles = req.body;
    var addedArticles = [];

    newArticles.forEach( ( item, index ) => {
      newArticleCreate(item).save().then(
        addedArticle => {
        addedArticles.push(addedArticle);
        console.log( 'added article -> ', addedArticle.href );
        if( index == newArticles.length - 1 ) {
            console.log( 'number of added articleS -> ', addedArticles.length );
            res.send(addedArticles);
        }
      }, err => { res.send(err); });
    })

  });



  app.post('/article/add', (req, res) => {

    // .then((_tags)=>{
    //   // console.log('articles cntrl -> ', _tags );
    // });

    newArticleCreate(req.body).save().then(
      doc => {
        console.log('add article -> ', doc.href);
        ArticleTagsService.add(doc.tags);
        res.send(doc);
      }, err => {
        res.send(err);
      });
  });


  app.get('/articles/:statusfilter/:pageSize/:pageNumber', (req,res ) => {
    // status filter -> all, staging, live
    console.log( 'get articles ');
    var statusFilter = {};
    var statusName = req.params.statusfilter.replace(/^\w/, c => c.toUpperCase());
    req.params.statusfilter === 'all' ? statusFilter = {} : statusFilter['status' + statusName ] = true;
    console.log('get-articles -> ',  statusFilter );

    Articles.find( statusFilter )
    .skip(parseInt(req.params.pageSize * (req.params.pageNumber - 1)) )
    .limit(parseInt(req.params.pageSize))
    .then((articles)=> {
      res.send(articles);
      console.log(statusFilter, 'get articles count -> ', articles.length);
    }, (err) => {
      console.log('error -> ', err);
      res.send(err);
    })
  });

  app.get('/article/:href', (req,res ) => {
    console.log('get Article');
    Articles.findOne({href: req.params.href}).then((article) => {
      res.send(article);
      console.log('article -> ', article.href);

    }, (err) => {
      res.send(err);
    })
  });


  app.post('/article/update/:href', (req,res ) => {
    var searchQuery = req.params.href;

    Articles.updateOne({href: searchQuery }, req.body ).then((article) => {
      console.log('update -> ', req.body, article);
      res.send(article);
    }, (err) => {
      res.send(err);
    })

  });

  app.get('/articles/search/:searchword?', (req,res ) => {
    // https://docs.mongodb.com/manual/reference/operator/query/text/#op._S_text
    var searchWord = req.params.searchword;
    var searchTagsArray = req.query.tags.split(' ');
    var searchOptions = null;
    console.log('search ' , searchWord, ' / ',  searchTagsArray, );

    if(!searchWord){ // if no searchWord, only Tags are considered in search
      searchOptions = {
        $or: [
          { tags: { $in: searchTagsArray } },
        ],
      };
    }else {
      searchOptions = {
        $or: [
          { href : { "$regex": searchWord , "$options": "i" } },
          { headline: { "$regex": searchWord , "$options": "i" } },
          { description: { "$regex": searchWord , "$options": "i" }},
          { vidDesc: { "$regex": searchWord , "$options": "i" }},
          { sources: { "$regex": searchWord , "$options": "i" }},
          // { tags: { "$regex": searchTags , "$options": "i" }},
          { tags: { $in: searchTagsArray } },
        ],
      };
    }

    Articles.find(searchOptions).then((articles) => {

      console.log( 'articles num -> ',  articles.length );

      res.send(articles)
    }, (err) => { res.send(err) })
  });

};
