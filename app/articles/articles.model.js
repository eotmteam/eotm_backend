const mongoose = require('mongoose');

const timeOfCreation = () => {
  var date = new Date().getTime();
  return date;
};

const createGallery = (galleryLenght, galleryFormat) => {
  console.log('galery -> ', galleryLenght, galleryFormat);
  var array = [['01.jpg', '01s.jpg'], ['02.jpg', '02s.jpg'], ['03.jpg', '03s.jpg']];
  if(!galleryLenght) array = [];
  return array;
};

const ArticleModelOpt = {
  unique : {
    type: Boolean,
    default: false
  },
  href : {
    type: String,
    required: true,
    minlength: 3,
    maxlength: 50,
    trim: true,
    unique: true
  },
  category: {
    type: String,
    // required: true
  },
  subcategory : {
    type: String,
    // required: true
  },
  tags: {
    type: Array,
  },
  thumb: {
    picture : {
      type: String,
      trim: true
      // required: true,
    },
    description:{
      type: String,
      trim: true,
      required: true,
    }
  },
  content: {
    title: {
      type: String,
      maxlength: 50,
      // required: true,
    },
    description: {
      type: String,
      default: null
    },
    video: {
      desciption : {
        type: String,
        default: null
      },
      url : {
        type: String,
        default: null
      },
    },
    gallery : {
      description: {
        type: String,
        default: null
      },
      mainPicture : {
        type: String,
        default: false
      },
      length : {
        type: Number,
        default: null
      },
      format : {
        type: String,
        default: '.jpg'
      },
      content: {
        type: Array,
        default: []
        // default: createGallery(this.length, this.format)
      }
    },
    sources : {
      type: Array,
      default: null
    }
  },
  date : {
    type: Date,
    default: timeOfCreation()
  },
  status: {
    ready : {
      type: Boolean,
      default: false
    },
    staging : {
      type: Boolean,
      default: false
    },
    live : {
      type: Boolean,
      default: false
    }
  }

};

var Articles = mongoose.model('Article' , ArticleModelOpt );

// var ArticleReady = mongoose.model('ReadyArticle' , ArticleModelOpt );
// var ArticleAll = mongoose.model('AllArticle' , ArticleModelOpt );
// var ArticleLive = mongoose.model('LiveArticle' , ArticleModelOpt );

module.exports = { Articles };
