
// const { ArticleTagsService } = require('./tags.service.js')

module.exports = function( app, ArticleTagsService ){

  app.get('/articles/tags/all', function(req, res){
    ArticleTagsService.get().then((_tags)=> {
      res.send(_tags);
      console.log('get tags from controller -> ', _tags );
    }, (err) => {
      console.log('error -> ', err);
      res.send(err);
    })
  });

}
