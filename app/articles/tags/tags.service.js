const { ArticleTags } = require('./tags.model.js');

const ArticleTagsService = {
  get: () => {
   return  ArticleTags.find().then((_tags)=>{
      console.log('get all tags -> ', _tags);
      return _tags;
    });
  },
  add: (_tags) => {

    const addTag = (_name) => {
      new ArticleTags({
        name: _name
      }).save().then( _newTag => console.log( 'tag added -> ', _newTag) );
    };

    for (var i = 0; i < _tags.length; i++) {
      let _newTag = _tags[i];
      ArticleTags.countDocuments( { name: _newTag } ).then((_count)=>{
        if(_count < 1){
          addTag(_newTag);
        }
      });
    };

  },
  search: (_query) => {
   return ArticleTags.find({ name: _query}).then((_tags)=>{
     return _tags;
   });
  }
};

module.exports = { ArticleTagsService };
